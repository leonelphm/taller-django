from django.contrib import messages
from django.contrib.auth.models import User
from django.shortcuts import redirect


def verifyUsers(function):
    """!
    Decorador que verifica si existen usuarios almacenados en la plataforma

    @author Ing. Leonel P. Hernandez M. (lhernandez at analiticom.com)
    @copyright ANALITICOM
    @date 08-04-2019
    @version 1.0.0
    """
    def wrap(request, *args, **kwargs):
        """
        Función de contenedor en la que se llama los argumento
        La función interna puede acceder a las funciones locales externas como en este caso "function"
        """
        if  User.objects.all():
            print("existe")
            return function(request, *args, **kwargs)
        else:
            print("error")
            messages.error(request, "No existen usuario, para listar, puede registrar")
            return redirect('users:registrar')
    return wrap