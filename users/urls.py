from django.urls import path
from .views import *

app_name = 'users'
urlpatterns = [
    path('hola/', hola, name='hola'),
    path('', inicio, name='inicio'),
    path('registrar-usuario', registerUser, name='register'),
    path('usuario-registrar', registrarUsuario, name='registrar'),
    path('usuarios-listar', listUsers, name='list'),
    path('usuario-actualizar/<int:pk>/', updateUser, name='update'),
    path('usuario-eliminar/<int:pk>/', deleteUser, name='delete'),
    path('usuarios-detalle-json/<int:pk>/', detailUserJson, 
        name='detail-json'),
    #: Vistas Genericas
    path('hola-mundo', HolaViews.as_view(), name='hola_mundo'),
    path('inicio-vista-generica', InicioViews.as_view(), 
        name='inicio_generico'),
    path('registro', CreateUserProfileView.as_view(), 
        name='registro_generico'),
    path('listar', ListUsersView.as_view(), name='listar_generico'),
    path('actualizar/<int:pk>/', UserUpdateView.as_view(), 
        name='actualizar_generico'),
    path('actualizar-perfil/<int:pk>/', UserProfileUpdateView.as_view(), 
        name='actualizar_perfil_generico'),
    path('eliminar-usuario/<int:pk>/', UserDeleteView.as_view(), 
        name='eliminar_generico'),
]
