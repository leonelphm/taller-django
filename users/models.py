"""
ANALITICOM taller-django
"""
## @package users.models
#
# Model contiene los modelos de datos del users
# @author Team Analiticom-Mérida
# @date 08-04-2019
# @version 1.0

from django.db import models
from django.contrib.auth.models import User


class UserProfile(models.Model):
    """!
    Class modelo para almacenar los registro del pefil de usuario

    @author Ing. Leonel P. Hernandez M. (lhernandez at analiticom.com)
    @copyright ANALITICOM
    @date 08-04-2019
    @version 1.0.0
    """
    def get_upload_to(self, filename):
        return "perfil/%s/%s" % (self.fk_user.username, filename)
    #: Campo one to one a la tabla usuer de django
    fk_user = models.OneToOneField(User, on_delete=models.CASCADE)
    id_perfil = models.PositiveIntegerField(verbose_name='Documento de identidad', unique=True)
    foto = models.FileField(upload_to=get_upload_to, null=True, blank=True)
    descripcion = models.TextField(null=True, blank=True)

    class Meta:
        """!
            Clase meta data del modelo
        """
        ordering = ('fk_user',)
        verbose_name = 'User Profile'
        verbose_name_plural = 'Users Profiles'
        db_table = 'user_profile'

    def __str__(self):
        """!
        Function that shows the user's profile

        @return Devuelve el username del usuario
        """
        return self.fk_user.username
