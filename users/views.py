
"""
ANALITICOM taller-django
"""
## @package users.views
#
# View que contiene los controladores de los procesos de users
# @author Team Analiticom-Mérida
# @date 08-04-2019
# @version 1.0

import json
from django.contrib import messages
from django.contrib.auth import password_validation
from django.contrib.auth.models import User
from django.contrib.messages.views import SuccessMessageMixin
from django.core import serializers
from django.core.paginator import Paginator
from django.http import (
    HttpResponse, JsonResponse
)
from django.template import loader
from django.shortcuts import (
    render, get_object_or_404, redirect
)
#: Vistas genericas
from django.views import View
from django.views.generic import (
    TemplateView, ListView
)
from django.views.generic.detail import DetailView
from django.views.generic.edit import (
    UpdateView, DeleteView, FormView
)
from django.urls import reverse_lazy

from .decorators import verifyUsers
from .forms import *
from .models import *



def hola(request):
    """
    Funcion que responde Hola Mundo!
    
    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    """
    if request.method == 'GET':
        # desarrollar la logica del metodo
        return HttpResponse('Hola Mundo!')


def inicio(request, templates='inicio.html'):
    """
    Funcion que responde el template inicial del taller
    
    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    @params request objeto de la Peticion Http
    @params templates plantilla donde se rendreiza la respuesta
    @return objeto de la respuesta http
    """
    template = loader.get_template(templates)
    return HttpResponse(template.render({}, request))


def registerUser(request, templates='users/users.register.html'):
    """
    Funcion que responde el template para registrar un usuario, ademas resuelve la peticion por post cuando se llena el formulario
    
    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    @params request objeto de la Peticion Http
    @params templates plantilla donde se rendreiza la respuesta
    @return objeto de la respuesta http
    """
    template = loader.get_template(templates)
    if request.POST:
        username = request.POST.get('username')
        first_name = request.POST.get('first_name')
        last_name = request.POST.get('last_name')
        email = request.POST.get('email')
        password1 = request.POST.get('password1')
        password2 = request.POST.get('password2')
        id_perfil = request.POST.get('id_perfil')
        foto = request.FILES
        descripcion = request.POST.get('descripcion')
        #: comprobar si existe o no el username 
        try:
            User.objects.get(username=username)
            messages.error(request, 
                           'Error el username: {0}, ya existe'.format(username)
                          )
            return HttpResponse(template.render({}, request))
        except:
            pass
        #: comprobar si existe o no el email 
        try:
            User.objects.get(email=email)
            messages.error(request, 
                           'Error el email: {0}, ya existe'.format(email))
            return HttpResponse(template.render({}, request))
        except:
            pass
        #: validar si las contraseñas son iguales
        if password1 == password2:
            #: validar password
            try:
                password_validation.validate_password(password1)
            except Exception as e:
                messages.error(request, ' '.join(e))
                return HttpResponse(template.render({}, request))
        else:
            messages.error(request, 'Error en las contraseña no son iguales')
            return HttpResponse(template.render({}, request))

        try:
            new_user = User.objects.create_user(
                username=username,
                first_name=first_name,
                last_name=last_name,
                email=email,
                password=password1,
                )
            new_user.set_password(password1)
            new_user.save()
        except Exception as e:
            messages.error(request, e)
            pass
        if foto:
            try:
                userprofile = UserProfile(
                    id_perfil=id_perfil,
                    descripcion=descripcion
                    )
                userprofile.fk_user = new_user
                userprofile.foto.save(foto['foto'].name, 
                                      foto['foto'].file, save=True)
                messages.success(request, 'Registro exitoso')
            except Exception as e:
                messages.error(request, e)
                pass
    return HttpResponse(template.render({}, request))


def registrarUsuario(request, templates='users/registrar.usuario.html', 
                     form=UserForm, form_profil=UserProfileForm):
    """
    Funcion que responde el template para registrar un usuario, ademas
    resuelve la peticion por post cuando se llenan los formularios
    
    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    @params request objeto de la Peticion Http
    @params templates plantilla donde se rendreiza la respuesta
    @params form objeto que contiene el formulario del usuario
    @params form_profil objeto que contiene el formulario del perfil de usuario
    @return objeto de la respuesta http
    """
    if request.POST:
        form = UserForm(request.POST)
        form_profil = UserProfileForm(request.POST, request.FILES)
        if form.is_valid():
            new_user = form.save()
            if form_profil.is_valid():
                profile = form_profil.save(commit=False)
                profile.fk_user = new_user
                try:
                    profile.save()
                    messages.success(request, 'Registro exitoso')
                except Exception as e:
                    messages.error(request, 'Algo ocurre: {0}'.format(e))
        
    return render(request, templates, {'form': form, 
                                       'form_profil':form_profil})


@verifyUsers
def listUsers(request):
    """
    Funcion que lista los usuarios registrados 
    
    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    @params request objeto de la Peticion Http
    @return objeto de la respuesta http
    """
    user_list = User.objects.select_related()
    paginator = Paginator(user_list.order_by('pk'), 2) # Show 10 users per page

    page = request.GET.get('page')
    users = paginator.get_page(page)
    return render(request, 'users/list.users.html', {'users': users})


def updateUser(request, form=UpdateUserForm, 
               form_profil=UserProfileForm, *args, **kwargs):
    """
    Funcion que actualiza un usuario seleccionado
    
    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    @params request objeto de la Peticion Http
    @params form objeto que contiene el formulario del usuario
    @params form_profil objeto que contiene el formulario del perfil de usuario
    @return objeto de la respuesta http
    """
    instance = get_object_or_404(User, pk=kwargs['pk'])
    form = form(request.POST or None, instance=instance)
    instance_profile = get_object_or_404(UserProfile, fk_user=kwargs['pk'])
    form_profil = UserProfileForm(request.POST or None,
                                  request.FILES or None, 
                                  instance=instance_profile)
    if request.POST:
        if form.is_valid():
            new_user = form.save()
            if form_profil.is_valid():
                profile = form_profil.save(commit=False)
                profile.fk_user = new_user
                try:
                    profile.save()
                    messages.success(request, 'Registro exitoso')
                except Exception as e:
                    messages.error(request, 'Algo ocurre: {0}'.format(e))
    return render(request, 'users/update.user.html', 
                 {'form': form, 'form_profil':form_profil})


def deleteUser(request, *args, **kwargs):
    """
    Funcion que elimina un usuario seleccionado
    
    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    @params request objeto de la Peticion Http
    @return objeto de la respuesta http
    """
    user = get_object_or_404(User, pk=kwargs['pk'])
    user_profile = get_object_or_404(UserProfile, fk_user=kwargs['pk'])
    if request.POST:
        user.delete()
        user_profile.delete()
        messages.success(request, 'Se elimino con exitoso el usuario')
        user_list = User.objects.select_related()
        return redirect('users:list')
    return render(request, 'users/delete.user.html', {'user':user})


def detailUserJson(request, *args, **kwargs):
    """
    Funcion que muestra el detalle de un usuario seleccionado
    
    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    @params request objeto de la Peticion Http
    @return objeto Json con la respuesta
    """
    user = get_object_or_404(User, pk=kwargs['pk'])
    user_profile = get_object_or_404(UserProfile, fk_user=kwargs['pk'])
    
    serialized_object = serializers.serialize('json', [user])
    return JsonResponse(json.loads(serialized_object), safe=False)


#: Vistas basadas en clases

class HolaViews(View):
    """
    Vista basada en clase para controlar el metodo get
    """
    def get(self, request):
        """
        Metodo get, responde Hola Mundo!

        @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
        @date 08-04-2019
        @version 1.0
        """
        # desarrollar la logica del metodo
        return HttpResponse('Hola Mundo!')


class InicioViews(TemplateView):
    """
    Vista basada en clase para responder a un plantilla

    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    """
    # Atributo Template de la clase 
    template_name = "inicio.html"


class CreateUserProfileView(FormView):
    """"
    Vista generica basada en clase para manipular un o varios formularios

    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    """
    # Atributo Template de la clase 
    template_name = "users/registrar.usuario.html"
    # Atributo objecto del formaulario
    form_class = UserForm
    # Atributo definido por el desarrollador para manejar otro formulario
    form_profile = UserProfileForm
    # Atributo para redirigir a la ruta cuando se cumple el registro
    success_url = "registro"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form_profil'] = self.form_profile
        return context

    def form_valid(self, form):
        new_user = form.save()
        form_profil = self.form_profile(self.request.POST, self.request.FILES)
        if form_profil.is_valid():
            profile = form_profil.save(commit=False)
            profile.fk_user = new_user
            try:
                profile.save()
                messages.success(self.request, 'Registro exitoso')
            except Exception as e:
                messages.error(self.request, 'Algo ocurre: {0}'.format(e))
        return super().form_valid(form)


class ListUsersView(ListView):
    """"
    Vista basada en clase para Listar los registro de un modelo

    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    """
    model = User
    template_name = 'users/list.users.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'users'  # Default: object_list
    paginate_by = 2
    queryset = User.objects.select_related().order_by('pk')  # Default: Model.objects.all()


class UserUpdateView(SuccessMessageMixin, UpdateView):
    """"
    Vista generica basada en clase para actualizar el registro de un modelo

    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    """
    model = User
    fields = ['username', 'first_name', 'last_name']
    template_name = 'users/user_update_form.html'
    success_url = "/"
    success_message = 'Se actualizo el usuario con exito'


class UserProfileUpdateView(SuccessMessageMixin, UpdateView):
    """"
    Vista generica basada en clase para actualizar el registro de un modelo

    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    """
    model = UserProfile
    fields = ['id_perfil', 'foto', 'descripcion']
    template_name = 'users/user_profile_update_form.html'
    success_url = "/"
    success_message = 'Se actualizo el perfil del usuario con exito'


class UserDeleteView(DeleteView):
    """"
    Vista generica basada en clase para eliminar el registro de un modelo

    @author Leonel P. Hernandez M. (lhenandez at analiticom.com)
    @date 08-04-2019
    @version 1.0
    """
    model = User
    success_url = reverse_lazy('users:list')
    template_name = 'users/user_confirm_delete.html'
