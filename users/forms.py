from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.forms import ModelForm

from .models import *


class UserForm(UserCreationForm):
    """!
    Class que crea el formulario del usuario

    @author Ing. Leonel P. Hernandez M. (lhernandez at analiticom.com)
    @copyright ANALITICOM
    @date 08-04-2019
    @version 1.0.0
    """
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2',
                  'first_name', 'last_name', 'email']

        def __init__(self, arg):
            super(UserForm, self).__init__()
            self.fields['first_name'].widget.attrs.update({'placeholder':
                                                           'Nombres'})
            self.fields['first_name'].required = True
            self.fields['last_name'].widget.attrs.update({'placeholder':
                                                          'Apellidos'})
            self.fields['last_name'].required = True
            self.fields['username'].widget.attrs.update({'placeholder':
                                                         'Nombre de usuario \
                                                         (Username)'})
            self.fields['username'].required = True
            self.fields['password1'].widget.attrs.update({'placeholder':
                                                          'Contraseña'})
            self.fields['password1'].required = True
            self.fields['password2'].widget.attrs.update({'placeholder':
                                                          'Repite la Contraseña'})

            self.fields['password2'].required = True
            self.fields['email'].widget.attrs.update({'class': 'form-control',
                                                      'placeholder': 'Email'})
            self.fields['email'].required = True

    def clean(self):
        cleaned_data = super(UserForm, self).clean()
        email = cleaned_data.get("email")

        if email:
            msg = "Error este email: %s, ya se encuentra asociado a una cuenta\
                  " % (email)
            try:
                User.objects.get(email=email)
                self.add_error('email', msg)
            except:
                pass


class UpdateUserForm(UserCreationForm):
    """!
    Class que crea el formulario del perfil de usuario

    @author Ing. Leonel P. Hernandez M. (lhernandez at analiticom.com)
    @copyright ANALITICOM
    @date 08-04-2019
    @version 1.0.0
    """
    class Meta:
        model = User
        fields = ['username', 'password1', 'password2',
                  'first_name', 'last_name', 'email']

        def __init__(self, arg):
            super(UserForm, self).__init__()
            self.fields['first_name'].widget.attrs.update({'placeholder':
                                                           'Nombres'})
            self.fields['first_name'].required = True
            self.fields['last_name'].widget.attrs.update({'placeholder':
                                                          'Apellidos'})
            self.fields['last_name'].required = True
            self.fields['username'].widget.attrs.update({'placeholder':
                                                         'Nombre de usuario \
                                                         (Username)'})
            self.fields['username'].required = True
            self.fields['password1'].widget.attrs.update({'placeholder':
                                                          'Contraseña'})
            self.fields['password1'].required = True
            self.fields['password2'].widget.attrs.update({'placeholder':
                                                          'Repite la Contraseña'})

            self.fields['password2'].required = True
            self.fields['email'].widget.attrs.update({'class': 'form-control',
                                                      'placeholder': 'Email'})
            self.fields['email'].required = True

class UserProfileForm(ModelForm):

    class Meta:
        model = UserProfile
        exclude = ['fk_user']

        def __init__(self, arg):
            super(UserProfileForm, self).__init__()
            self.fields['id_perfil'].widget.attrs.update({'placeholder':
                                                           'Identificador'})
