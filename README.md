# taller-django

Para instalar la aplicación en modo desarrollo debera seguir los siguientes pasos:
==================================================================================

1-) Instalar el controlador de versiones git:
---------------------------------------------

    Ingresar como super usuario:

    $ su

    # aptitude install git
    
    Salir del modo super usuario

2-) Descargar el codigo fuente del taller-django :
--------------------------------------------------

    Para descargar el código fuente del taller contenido en su repositorio GIT realice un clon del taller-django:

    $ git clone https://gitlab.com/leonelphm/taller-django.git


3-) Crear un Ambiente Virtual:
------------------------------

El taller está desarrollado con el lenguaje de programación Python, se debe instalar Python v3.5 o mayor Con los siguientes comandos puede instalar Python y PIP.

    Entrar como root o super usaurio para la instalacion 

    # aptitude install python3.5 python3-pip python3.5-dev python3-setuptools

    # aptitude install python3-virtualenv virtualenvwrapper

    Salir del modo root y crear el ambiente:

    $ mkvirtualenv --python=/usr/bin/python3 taller-django


4-) Instalar los requerimientos del taller:
-------------------------------------------

    Para activar el ambiente virtual "taller-django" ejecute el siguiente comando:

    $ workon taller-django

    Con el comando anterio se activa el ambiente virtual quedando de la siguiente manera:

    (taller-django)$

    Entrar en la carpeta raiz del proyecto:

    (taller-django)$ cd taller-django

    (taller-django)taller-django$ 

    Desde ahi se deben instalar los requirimientos del proyecto con el siguiente comando:

    (taller-django)taller-django$ pip install -r requerimientos.txt

    De esta manera se instalaran todos los requerimientos iniciales para montar el taller


5-) Migrar los modelos:
-----------------------

    Para migrar los modelos se debe ejecutar los siguientes comandos:

    Primero se debe crear la migracion

    $ python manage.py makemigrations users

    y luego migrar:

    $ python manage.py migrate


6-) Inicio de la plataforma:
----------------------------

    (taller-django)taller-django$ python manage.py runserver
    
    Se despliega la plataforma en el puerto 8000 

    Puede iniciar el navegador de su preferencia e ingresar en la url la siguiente direccion:

    http://localhost:8000  
